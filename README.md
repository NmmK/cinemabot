# CinemaBot
# Telegram-бот на Python.

*Исхаков Тимур*

## Описание

В проекте предусмотрена функция поиска фильмов, а также генерация случайного фильма. 

При создании бота были использованы API:

- pyTelegramBotAPI

- KinopoiskApiUnofficial

Ссылки:

 - https://github.com/eternnoir/pyTelegramBotAPI

 - https://kinopoiskapiunofficial.tech/
